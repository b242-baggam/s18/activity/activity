// 3. Create a function which will be able to add two numbers.

//  - Numbers must be provided as arguments.
//  - Display the result of the addition in our console.
//  - function should only display result. It should not return anything.

function numAdd(num1, num2){
    let sum = num1 + num2;
    console.log(sum);
}
console.log("Displayed sum of 5 and 15");
numAdd(5,15);

// 4. Create a function which will be able to subtract two numbers.
function numSub(num1, num2){
    let diff = num1 - num2;
    console.log(diff);
}
console.log("Displayed difference of 20 and 5");
numSub(20,5);

//  - Numbers must be provided as arguments.
//  - Display the result of subtraction in our console.
//  - function should only display result. It should not return anything.

// 5. Invoke and pass 2 arguments to the addition function

// 6. Invoke and pass 2 arguments to the subtraction function

// 7. Create a function which will be able to multiply two numbers.
function numMul(num1, num2){
    let mul = num1 * num2;
    return mul;
}
console.log("The product of 50 and 10")
let product = numMul(50,10);
console.log(product);
//  - Numbers must be provided as arguments.
//  - Return the result of the multiplication.

// 8. Create a function which will be able to divide two numbers.
function numDiv(num1, num2){
    let quo = num1/num2;
    return quo;
}
console.log("The quotient of 50 and 10:");
let quotient = numDiv(50,10);
console.log(quotient);
//  - Numbers must be provided as arguments.
//  - Return the result of the division.

// 9. Create a new variable called product.

//  - This product variable should be able to receive and store the result of multiplication function.

// 10. Create a new variable called quotient.

//  - This quotient variable should be able to receive and store the result of division function.
//  - Log the value of product variable in the console.
//  - Log the value of quotient variable in the console.

// 11. Create a function which will be able to get total area of a circle from a provided radius.
function area(radius){
    let area = Math.PI * (radius ** 2);
    return area;
}
let circleArea = area(15);
console.log("The result of getting the area of a circle with 15 radius:");
console.log(circleArea);

//  - a number should be provided as an argument.
//  - look up the formula for calculating the area of a circle with a provided/given radius.
//  - look up the use of the exponent operator.
//  - return the result of the area calculation.

// 12. Create a new variable called circleArea.

//  - This variable should be able to receive and store the result of the circle area calculation.
//  - Log the value of the circleArea variable in the console.

// 13. Create a function which will be able to get total average of four numbers.
function average(num1, num2, num3, num4){
    average = (num1 + num2 + num3 + num4)/4;
    return average;
}
let averageVar = average(20,40,60,80);
console.log("The average of 20,40,60 and 80:");
console.log(averageVar);

//  - 4 numbers should be provided as an argument.
//  - look up the formula for calculating the average of numbers.
//  - return the result of the average calculation.

// 14. Create a new variable called averageVar.

//  - This variable should be able to receive and store the result of the average calculation
//  - Log the value of the averageVar variable in the console.

// 15. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
function POrF(awarded,total){
    let percentage = (awarded/total)*100;
    let isPassed = (percentage>75);
    return isPassed;
}
let isPassingScore = POrF(38,50);
console.log("Is 38/50 a passing score?");
console.log(isPassingScore);
//  - this function should take 2 numbers as an argument, your score and the total score.
//  - First, get the percentage of your score against the total. You can look up the - formula to get percentage.
//  - Using a relational operator, check if your score percentage is bthan 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
//  - return the value of the variable isPassed.
//  - This function should return a boolean.

// 16. Create a new variable called isPassingScore.

//  - This variable should be able to receive and store the boolean result of the function.
//  - Log the value of the isPassingScore variable in the console.
